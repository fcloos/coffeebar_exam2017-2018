#! /usr/bin/env python3
# coding: utf-8
from uuid import uuid4
import pandas as pd
import random

class BasicClient:
    """
    Basic customer
    """
    def __init__(self):
        # generate a unique ID
        self.client_id = "CID" + str(int(random.random() * 1000000000))
        self.budget = 0
        self.proba_food = {}
        self.proba_drink = {}

    def set_proba_drink(self, stat_drink):
        self.proba_drink = stat_drink

    def set_proba_food(self, stat_food):
        self.proba_food = stat_food

    def get_proba_drink(self):
        if not self.proba_drink:
            self._probabilite()
        return self.proba_drink

    def get_proba_food(self):
        if not self.proba_food:
            self._probabilite()
        return self.proba_food

    def get_budget(self):
        return self.budget

    def _probabilite(self):
        filepath = "Data/Coffeebar_2013-2017.csv"
        df = pd.read_csv(filepath, sep=';')
        informations = df.to_dict('records')
        stat_hour_drinks = {}
        stat_hour_food = {}
        for ligne in informations:
            food = ligne.get('FOOD', '')
            if str(food) == 'nan':
                food = 'Nothing'
            drink = ligne.get('DRINKS', '')
            if str(drink) == 'nan':
                drink = 'Nothing'
            hour = ligne.get('TIME', '')
            # Part sale/hour
            # We take only the 5 last string from hour because we just want the hour.
            hour = hour[11:]
            # retrieving the sub-dict corresponding to a specific hour for the drinks (next line idem for foods)
            hour_drinks = stat_hour_drinks.get(hour, {})
            hour_foods = stat_hour_food.get(hour, {})

            total_drink = hour_drinks.get(drink, 0) + 1
            total_food = hour_foods.get(food, 0) + 1

            # Updating dictionaries with the new values.
            hour_drinks.update({
                drink: total_drink,
            })
            hour_foods.update({
                food: total_food,
            })
            stat_hour_drinks.update({
                hour: hour_drinks,
            })
            stat_hour_food.update({
                hour: hour_foods,
            })
        self.proba_food = stat_hour_food
        self.proba_drink = stat_hour_drinks

    def get_drink(self, hour):
        if not self.proba_drink:
            self._probabilite()

        stat_drink_hours = self.proba_drink
        # stat is the sub-dict of the key hour in the stat_drink_hours dict
        stat = stat_drink_hours.get(hour, {})
        values = stat.values()
        total = sum([nb for nb in values])
        probs = {}
        drinks = {}
        cpt = 1
        # getting the key value pair of each key of the stat dict
        # name : type of drinks /  nb : quantity of 'name'
        for name, nb in stat.items():
            # drinks.keys contains the type of drinks
            # if not in the dictionary, we add the type to the dict
            if name not in drinks.keys():
                drinks.update({
                    cpt: name,
                })
                # cpt : just an index
                cpt += 1
            # setting prob to zero
            prob = 0
            if total:
                prob = nb / float(total)
            # adding the buying probability of the name item in the probability dictionary
            probs.update({
                name: prob,
            })
        result = ''
        # random.choice function return an item X with a corresponding probability from Y
        if probs:
            # random.choice (X , Y)
            result = random.choices(list(drinks.keys()), list(probs.values()))[
                0]
        return drinks.get(result, 'Nothing')

    def get_food(self, hour):
        if not self.proba_food:
            self._probabilite()

        stat_hours = self.proba_food
        stat = stat_hours.get(hour, {})
        values = stat.values()
        total = sum([nb for nb in values])
        probs = {}
        foods = {}
        cpt = 1
        # loop similar from get_drink but with food.
        for name, nb in stat.items():
            if name not in foods.keys():
                foods.update({
                    cpt: name,
                })
                cpt += 1
            prob = 0
            if total:
                prob = nb / float(total)
            probs.update({
                name: prob,
            })
        result = ''
        if probs:
            result = random.choices(list(foods.keys()), list(probs.values()))[
                0]
        return foods.get(result, 'Nothing')

    def pay(self, drink, food, date):
        amount = 0
        price_food = {
            'sandwich': 5,
            'cookie': 2,
            'Nothing': 0,
        }
        price_drinks = {
            'milkshake': 5,
            'frappucino': 4,
            'water': 2,
            'Nothing': 0,
        }
        # Calculating the amount of food or drink
        # if the food have not a price over, the price is 3.
        amount += price_drinks.get(drink, 3)
        amount += price_food.get(food, 3)
        # Removing this price from the budget
        # Additional control for part 4 question 3
        if date[:4] < "2015":
            self.budget -= amount
        else:
            self.budget -= amount*1.2

    def get_client_id(self):
        return self.client_id


class UniqueClient(BasicClient):
    """
    Regular once customer
    """
    def __init__(self):
        super().__init__()
        self.budget = 100


class TripAdvisorClient(UniqueClient):
    """
    Regular customer by TripAdvisor
    """

    def _get_tip(self):
        return round(random.uniform(0, 10))

    def pay(self, drink, food, date):
        super().pay(drink, food, date)
        # removing the tip from the budget.
        self.budget -= self._get_tip()

# Creating sub-classes and (sub-)sub-classes from BasicClient
class ReturningClient(BasicClient):
    """
    Returning customer (250+ budget)
    """
    def __init__(self):
        super().__init__()
        self.drinks_history = {}
        self.foods_history = {}
        self.budget = 250

    def get_drinks_history(self):
        return self.drinks_history

    def get_foods_history(self):
        return self.foods_history

    def pay(self, drink, food, date):
        super().pay(drink, food, date)
        self.foods_history.update({
            date: food,
        })
        self.drinks_history.update({
            date: drink,
        })



class HipsterClient(ReturningClient):
    """
    Returning customer (500+ budget)
    """
    def __init__(self):
        super().__init__()
        self.budget = 500


