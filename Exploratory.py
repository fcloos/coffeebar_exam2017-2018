import matplotlib.pyplot as plt
import pandas as pd
import plotly
import plotly.graph_objs as go
filepath = "Data/Coffeebar_2013-2017.csv"

def read_file():
    df = pd.read_csv(filepath, sep=';')
    lignes = df.to_dict('records')
    return lignes

#Programme principal
print("File reading")
lines = read_file()
# Lists initialization
foods = []
drinks = []
customers = []
Returning_customers = []

# Dictionaries initialization
total_foods = {}
total_drinks = {}
total_customers = {}
sale_hours = {}

# Implement a 'for' statement loop on each item of the list (lines)
nb_line = len(lines)
current_line = 0
print("Data computing")
for line in lines:
    # Data recovery : in the dictionary, we recover the item of the column.
    food = line.get('FOOD', '')
    if str(food) == 'nan':
        food = 'Nothing'
    drink = line.get('DRINKS', '')
    if str(drink) == 'nan':
        drink = 'Nothing'
    customer = line.get('CUSTOMER', '')
    hour = line.get('TIME', '')

    # If food is not still in the list (foods), we append it.
    if food not in foods:
        foods.append(food)
    # Same for drink
    if drink not in drinks:
        drinks.append(drink)
    # Same for customer + if it is several times in the list(customers), we append it to Returning_customers.
    if customer not in customers:
        customers.append(customer)
    else :
        Returning_customers.append(customer)

    # Counting part
    total_food = total_foods.get(food, 0) + 1
    total_drink = total_drinks.get(drink, 0) + 1
    total_customer = total_customers.get(customer, 0) + 1
    # Updating of the dictionary by the numbers found above
    total_foods.update({
        food: total_food,
    })
    total_drinks.update({
        drink: total_drink,
    })
    total_customers.update({
        customer: total_customer,
    })


    # Sells per hour part
    # In TIME we have the entire date : DD-MM-YY  hour:min
    # We take from 11th string of TIME because we just want the hours
    hour = hour[11:]
    # Recovery of the sales for each specific hour (according to the lines that we read)
    # Dictionary initialization :
    sale_this_hour = sale_hours.get(hour, {})
    # Recovery of the (sub-)dictionaries for the specific hour
    sale_this_hour_food = sale_this_hour.get('foods', {})
    sale_this_hour_drink = sale_this_hour.get('drinks', {})

    # Counting part
    total_drink = sale_this_hour_drink.get(drink, 0) + 1
    total_food = sale_this_hour_food.get(food, 0) + 1
    # Updating of the (sub-)dictionary by the numbers found above
    sale_this_hour_drink.update({
        drink: total_drink,
    })
    sale_this_hour_food.update({
        food: total_food,
    })

    total = sale_this_hour.get('Total', 0) + 1
    sale_this_hour.update({
        'foods': sale_this_hour_food,
        'drinks': sale_this_hour_drink,
        'Total': total,
    })
    sale_hours.update({
        hour: sale_this_hour,
    })
    current_line += 1

# Implementation of 'for' statement loop to find the unique customers
print(len(customers))

for c in Returning_customers:
    if c in customers:
        customers.remove(c)

# The size of customer list which counts only the unique customer
Nb_customers = len(customers)

# Using join function attached on a empty string to join the string between each list items
boissons = "\n\t- ".join([d for d in drinks if str(d) != 'nan'])
nourritures = "\n\t- ".join([f for f in foods if str(f) != 'nan'])

print("Part 1")
print("Number of Unique_customers: %s" % Nb_customers)
print("Foods:\n\t- %s" % nourritures)
print("Drinks:\n\t- %s" % boissons)

print("Part 2")
print("Total foods sold:")
# On the dictionaries, using items function to return each time the key and the respective item
# Implementation of for loop to have the key and the item.
# Implementation of for loop to have the key and the item.
food_name = []
food_total = []
for food, total in total_foods.items():
    food_name.append(food)
    food_total.append(total)
    print("\t- %s\t%s" % (food, total))

print("Total drinks sold")
drink_name = []
drink_total = []
for drink, total in total_drinks.items():
    drink_name.append(drink)
    drink_total.append(total)
    print("\t- %s\t%s" % (drink, total))

print("Part 3")
# 'For' loop to find the probabilities for each item per hour.
part4_proba_drink = {}
part4_proba_food = {}
for hour, sub_dict in sale_hours.items():
    print("%s" % hour)
    total = float(sub_dict.pop('Total', 1))
    for type_article, articles in sub_dict.items():
        if type_article == 'foods':
            for article, nb in articles.items():
                print("\t%s: %.2f%%" % (article, nb/total*100))
                # Additionnal code for the part4 statistic
                tmp = part4_proba_food.get(article, {})
                tmp.update({
                    hour: nb / total * 100,
                })
                part4_proba_food.update({
                    article: tmp,
                })
        else:
            for article, nb in articles.items():
                print("\t%s: %.2f%%" % (article, nb/total*100))
                # Additionnal code for the part4 statistic
                tmp = part4_proba_drink.get(article, {})
                tmp.update({
                    hour: nb/total*100,
                })

                part4_proba_drink.update({
                    article: tmp,
                })
# Bar plots
df = pd.read_csv(filepath, sep=';')

dff = df[['CUSTOMER', 'FOOD']]
ax = dff.groupby(['FOOD']).count().plot(kind='bar', title="Amount of each type of sold food over 5 years", color='red', edgecolor='red',legend=False, fontsize=9)
ax.set_xlabel("Food names", fontsize=8)
ax.set_ylabel("Total sales per food items (quantity)", fontsize=12)
plt.show()

dff = df[['CUSTOMER', 'DRINKS']]
ax = dff.groupby(['DRINKS']).count().plot(kind='bar', title="Amount of each type of sold drinks over 5 years", color='purple', edgecolor='purple',legend=False, fontsize=9)
ax.set_xlabel("Drink names", fontsize=3)
ax.set_ylabel("Total sales per drink items (quantity)", fontsize=12)
plt.show()


# Additionnal code for the part4 statistic
def _get_prob(tmp):
    proba_tmp = []
    for ent in tmp:
        proba_tmp.append(ent[1])
    return proba_tmp

def _get_hour(tmp):
    hour_tmp = []
    for ent in tmp:
        hour_tmp.append(ent[0])
    return hour_tmp

tmp_nothing = sorted(part4_proba_food.get("Nothing").items())
tmp_cookie = sorted(part4_proba_food.get("cookie").items())
tmp_muffin = sorted(part4_proba_food.get("muffin").items())
tmp_pie = sorted(part4_proba_food.get("pie").items())
tmp_sandwich = sorted(part4_proba_food.get("sandwich").items())

hour_proba = _get_hour(tmp_nothing)
Nothing_proba = _get_prob(tmp_nothing)
Cookie_proba = _get_prob(tmp_cookie)
Muffin_proba = _get_prob(tmp_muffin)
Pie_proba = _get_prob(tmp_pie)
Sandwich_proba = _get_prob(tmp_sandwich)


data_cookie = go.Bar(
            x=hour_proba,
            y=Cookie_proba,
            name = "Cookie"
        )

data_nothing = go.Bar(
            x=hour_proba,
            y=Nothing_proba,
            name = "Nothing"
        )

data_Muffin = go.Bar(
            x=hour_proba,
            y=Muffin_proba,
            name = "Muffin"
        )

data_Pie = go.Bar(
            x=hour_proba,
            y=Pie_proba,
            name = "Pie"
        )

data_Sandwich = go.Bar(
            x=hour_proba,
            y=Sandwich_proba,
            name = "Sandwich"
        )


layout_food = go.Layout(
    title="Proba buying Foods",
    xaxis=dict(
        title='Hours'
    ),
    yaxis=dict(
        title='Proba [%]'
    ),
    #bargap=0.2,
    barmode='group'
)

data = [data_nothing,data_Sandwich, data_cookie,data_Muffin,data_Pie]
plotly.offline.plot({
    "data": data,
    "layout": layout_food
})

# --------
tmp_frappucino = sorted(part4_proba_drink.get("frappucino").items())
tmp_soda = sorted(part4_proba_drink.get("soda").items())
tmp_coffee = sorted(part4_proba_drink.get("coffee").items())
tmp_tea = sorted(part4_proba_drink.get("tea").items())
tmp_water = sorted(part4_proba_drink.get("water").items())
tmp_milkshake = sorted(part4_proba_drink.get("milkshake").items())

Frap_proba = _get_prob(tmp_frappucino)
Soda_proba = _get_prob(tmp_soda)
Coffee_proba = _get_prob(tmp_coffee)
Tea_proba = _get_prob(tmp_tea)
Water_proba = _get_prob(tmp_water)
Milkshake_proba = _get_prob(tmp_milkshake)

data_Frap = go.Bar(
            x=hour_proba,
            y=Frap_proba,
            name = "Frappucino"
        )

data_Soda = go.Bar(
            x=hour_proba,
            y=Soda_proba,
            name = "Soda"
        )

data_Coffee = go.Bar(
            x=hour_proba,
            y=Coffee_proba,
            name = "Coffee"
        )

data_Tea = go.Bar(
            x=hour_proba,
            y=Tea_proba,
            name = "Tea"
        )

data_Water = go.Bar(
            x=hour_proba,
            y=Water_proba,
            name = "Water"
        )

data_Milkshake = go.Bar(
            x=hour_proba,
            y=Milkshake_proba,
            name = "Milkshake"
        )


layout_drink = go.Layout(
    title="Proba buying Drinks",
    xaxis=dict(
        title='Hours'
    ),
    yaxis=dict(
        title='Proba [%]'
    ),
    bargap=0.2,
    barmode='group'
)

data = [data_Frap,data_Soda,data_Coffee,data_Tea,data_Water,data_Milkshake]
plotly.offline.plot({
    "data": data,
    "layout": layout_drink
})

