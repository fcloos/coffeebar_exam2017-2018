Coffeebar simulation

Description

The goal of our repository is to better understand customers, to satisfy their requirements on the basis of the analyze of a data set. It should help the coffee bar to better manage its business, strategies.  


Running

When somebody wants to read the code, he should start running 'Exploratory' file first, then "Customers", "Test-Part2",  "Simulation" and finally "Part4".


Structure


« Exploratory" file investigates Coffeebar dataset and reveals some results and plots. 

In the second file, "Customers", takes the classes and sub-classes we’ve created related to the different kinds of customers : BasicClient ( with two attributes that sub-classes inherit: budget and ID), UniqueClient, ReturningClient, TripadvisorClient, HipsterClient. The Trip Advisor customer also has a tip as attribute. 

The file « Test_part2 » was created to test the class hierarchy from « Customers » file. 


In the file "Simulation", we simulate the activity of the coffeebar during 5 years and we make some plots that compare the simulation to the original dataset.


In the file "Part4", we analyze customers’ behavior based on the original dataset. After that,  we compare the original dataset to simulations that modify the original situation. 

