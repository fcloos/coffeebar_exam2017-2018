from Customers import UniqueClient
from Customers import TripAdvisorClient
from Customers import ReturningClient
from Customers import HipsterClient

if __name__ == "__main__":
    hour = '13:16:00'
    # Define a
    client_unique = UniqueClient()
    client_tripadvisor = TripAdvisorClient()
    client_returning = ReturningClient()
    client_hipster = HipsterClient()
    # Calculating the probabilities for the instance of the ClientUnique class
    proba_drink = client_unique.get_proba_drink()
    proba_food = client_unique.get_proba_food()
    # Giving the probabilities from the instance over to the others, instead of calculating again the probabilities
    client_tripadvisor.set_proba_drink(proba_drink)
    client_tripadvisor.set_proba_food(proba_food)
    client_returning.set_proba_drink(proba_drink)
    client_returning.set_proba_food(proba_food)
    client_hipster.set_proba_drink(proba_drink)
    client_hipster.set_proba_food(proba_food)
    for x in range(0, 10):
        print("Customer ID %s buy %s and %s at %s" % (client_unique.get_client_id(), client_unique.get_food(hour), client_unique.get_drink(hour), hour))
        print("Customer ID %s buy %s and %s at %s" % (client_tripadvisor.get_client_id(), client_tripadvisor.get_food(hour), client_tripadvisor.get_drink(hour), hour))
        print("Customer ID %s buy %s and %s at %s" % (client_returning.get_client_id(), client_returning.get_food(hour), client_returning.get_drink(hour), hour))
        print("Customer ID %s buy %s and %s at %s" % (client_hipster.get_client_id(), client_hipster.get_food(hour), client_hipster.get_drink(hour), hour))
