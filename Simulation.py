import matplotlib.pyplot as plt
import random
import pandas as pd
import plotly
import plotly.graph_objs as go

from Customers import UniqueClient
from Customers import TripAdvisorClient
from Customers import ReturningClient
from Customers import HipsterClient

filepath = "Data/Coffeebar_2013-2017.csv"

def generate_client_unique():
    number = random.uniform(0, 1)
    if number < 0.1:
        client = TripAdvisorClient()
    else:
        client = UniqueClient()
    return client

if __name__ == "__main__":

    df = pd.read_csv(filepath, sep=';')
    df.to_dict('records')
    # We take the column 'TIME' for each line (dict)
    dates = [line.get('TIME') for line in df.to_dict('records')]
    # Initializing list of returning customers
    returning_customer = []
    proba_drink = {}
    proba_food = {}
    for x in range(0, 999):  # generating the 1000 retourning customers
        number = random.uniform(0, 1)
        if number < 1 / 3:  #Third part of returning customers are hipster
            returning_customer.append(HipsterClient())
        else:
            returning_customer.append(ReturningClient())
    generate = []
    Income = []
    Income_date = 0
    count_day = 1
    for date in dates:
        if count_day % 171 == 0:
            Income.append(Income_date)
            Income_date = 0

        hour = date[11:]
        flag = True
        while flag:
            number = random.uniform(0, 1)
            if number < 0.2:  # probability returning customer
                returning_index = int(random.uniform(0, 999))
                client = returning_customer[returning_index]  # we are taking a returning customer from the returning list
            else:  # otherwise we generate a new unique customer
                client = generate_client_unique()
            if client.get_budget() >= 10:
                flag = False
        # ------- Valid for all iterations except the first one
        if proba_food:
            client.set_proba_food(proba_food)
        if proba_drink:
            client.set_proba_drink(proba_drink)
        drink = client.get_drink(hour)
        food = client.get_food(hour)
        # ------- We do this condition only for the first iteration
        # After that, proba_drink and proba_food are not NULL anymore
        if not proba_drink:
            proba_drink = client.get_proba_drink()
        if not proba_food:
            proba_food = client.get_proba_food()
        # --------
        Pre_Budget = client.get_budget()  # saving the budget of the client before paying
        client.pay(drink, food, date)
        Post_Budget = client.get_budget()  # saving the budget of the client after paying
        Income_date = Income_date + (Pre_Budget - Post_Budget)  # Computing what the client paied
        # Adding what the client payed to the income per day.

        # -------- Update the list generate
        generate.append({
            'TIME': date,
            'CUSTOMER': client.get_client_id(),
            'DRINKS': drink,
            'FOOD': food,
        })

        count_day += 1

    returning_client_example = returning_customer[150]
    print(returning_client_example.get_drinks_history())
    print(returning_client_example.get_foods_history())

    titres = ["TIME", "CUSTOMER", "DRINKS", "FOOD"]
    df = pd.DataFrame(generate, columns=titres)
    df.to_csv("exo3.csv")


# Bar plot - if prices change in 2015
df = pd.DataFrame(generate, columns=titres)
data = [go.Histogram(
    x=Income
)]

layout = go.Layout(
    title="Average income per day for an increasing price after 2015",
    xaxis=dict(
        title='Income'
    ),
    yaxis=dict(
        title='Number of days'
    ),
    bargap=0.2,
)

plotly.offline.plot({
    "data": data,
    "layout": layout
})



